**Table of contents**<br />

[[_TOC_]]

# Development
## Important notes
### unix2dos on data
After adding new files to `data`, make sure they are all in dos format:
```
find data/ -type f -print0 | xargs -0 unix2dos
```