[aigroup_2]
NormalCars

vehicles\VW_Golf_2\AI_VW_Golf_2.bus	7
vehicles\MB_W123_230E\AI_mb_w123_230e.bus	5
vehicles\MB_W123_230E\AI_mb_w123_230e_cab.bus	1
vehicles\VW_T3\VW_T3_Van.ovh	1
vehicles\Opel_Manta_B\ai_opel_manta_b.ovh	1
vehicles\Citr_BX\BX.ovh	4
[end]

[aigroup_2]
Trucks

vehicles\MAN_F90\AI_MAN_F90_Wechselbruecke.bus
[end]

[aigroup_2]
Commercials

vehicles\MB_T1\ai_mb_t1_kasten.ovh
vehicles\VW_T3\VW_T3_Transporter.ovh
[end]

[aigroup_2]
Ambulance

vehicles\MB_T1\ai_mb_t1_rtw.ovh	50
[end]